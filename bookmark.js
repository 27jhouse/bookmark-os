var OSJS = document.createElement('script');
OSJS.innerHTML = ` 


// creation args: title (string), icon(url string)
// open args: width, height, left, top (all are numbers)
var windows = 0;
function jswindow(args) {
  // specify options here
  var borderThickness = 2;
  var defaultWidth = 250;
  var defaultHeight = 150;
  var topClip = 0;
  var bottomClip = 24;
  var leftClip = 48;
  var rightClip = 24;

  // vWindow instead of this for event.target workaround
  var vWindow = this;
  
  function randInt(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }
  function clipRight() { return parseInt(vWindow.outerWindow.style.width) + parseInt(vWindow.outerWindow.style.left) + (borderThickness * 2) > window.innerWidth; }
  function clipBottom() { return parseInt(vWindow.outerWindow.style.height) + parseInt(vWindow.outerWindow.style.top) + (borderThickness * 2) > window.innerHeight; }
  
  this.setTitle = function(text) {
	  vWindow.outerWindow.bar.wname.textContent = text;
  }
  
  this.setIcon = function(url) {
	  vWindow.outerWindow.bar.icon.style.backgroundImage = 'url("' + url + '")';
  }
  
  // open window with optional width and height (will otherwise be default) and distances from the top left (otherwise random)
  this.open = function(args) {
	  vWindow.outerWindow.style.width = (args && args.width ? args.width + "px" : defaultWidth + "px");
	  vWindow.outerWindow.style.height = (args && args.height ? args.height + "px" : defaultHeight + "px");
	  vWindow.outerWindow.style.left = (args && args.left ? args.left + "px" : randInt(0, window.innerWidth - parseInt(vWindow.outerWindow.style.width)) + "px");
	  vWindow.outerWindow.style.top = (args && args.top ? args.top + "px" : randInt(0, window.innerHeight - parseInt(vWindow.outerWindow.style.height)) + "px");
	  vWindow.outerWindow.style.maxWidth = (window.innerWidth - (parseInt(vWindow.outerWindow.style.left) + (borderThickness * 2))) + "px";
	  vWindow.outerWindow.style.maxHeight = (window.innerHeight - (parseInt(vWindow.outerWindow.style.top) + (borderThickness * 2))) + "px";
	  document.body.appendChild(vWindow.outerWindow);
  }
  
  this.onclose = function() {}
  
  this.close = function() {
	  vWindow.outerWindow.remove();
	  vWindow.onclose();
  }
  
  // start constructor creation
  // make nodes
  windows++
  var ThisWin = windows;
  vWindow.outerWindow = document.createElement("div");
  vWindow.outerWindow.classList.add("window");
  vWindow.outerWindow.id = "window"+windows
  var WID = document.createElement("div")
  WID.id = windows;
  vWindow.outerWindow.appendChild(WID)

  vWindow.outerWindow.bar = document.createElement("div");
  vWindow.outerWindow.bar.classList.add("windowbar");
  
  if (args && args.icon) {
	  vWindow.outerWindow.bar.icon = document.createElement("span");
	  vWindow.outerWindow.bar.icon.classList.add("windowicon");
	  vWindow.outerWindow.bar.icon.style.backgroundImage = 'url("' + args.icon + '")';
	  vWindow.outerWindow.bar.appendChild(vWindow.outerWindow.bar.icon);
  }
  
  vWindow.outerWindow.bar.wname = document.createElement("span");
  vWindow.outerWindow.bar.wname.appendChild(document.createTextNode(args && args.title ? args.title : ""));
  vWindow.outerWindow.bar.wname.classList.add("windowtitle");
  
  vWindow.outerWindow.bar.close = document.createElement("span");
  vWindow.outerWindow.bar.close.appendChild(document.createTextNode(String.fromCharCode(10006)));
  vWindow.outerWindow.bar.close.classList.add("windowclose");
  vWindow.outerWindow.bar.close.title = "Close";

  if (args.WinType == "AppWin") {
	  vWindow.outerWindow.bar.close.onclick = function() { document.getElementById("Win"+ThisWin).remove(); AppWinOpen = false; vWindow.close(); windows--; console.log(windows); }
  }else {
  vWindow.outerWindow.bar.close.onclick = function() { document.getElementById("Win"+ThisWin).remove(); vWindow.close(); windows--; console.log(windows); }
  }
  vWindow.innerWindow = document.createElement("div");
  vWindow.innerWindow.classList.add("windowcontent");
  
  // icon already appended if specified
  vWindow.outerWindow.bar.appendChild(vWindow.outerWindow.bar.wname);
  vWindow.outerWindow.bar.appendChild(vWindow.outerWindow.bar.close);
  vWindow.outerWindow.appendChild(vWindow.outerWindow.bar);
  vWindow.outerWindow.appendChild(vWindow.innerWindow);
  
  // move window to front
  vWindow.outerWindow.addEventListener("mousedown", function(e) {
	  var allwindows = Array.from(document.querySelectorAll("div.window"));
	  if ((allwindows.indexOf(vWindow.outerWindow) != (allwindows.length - 1)) && e.target != vWindow.outerWindow.bar.close) document.body.appendChild(vWindow.outerWindow);
  }, false);
  
  // move window around
  var oldcursorX, oldcursorY;
  vWindow.outerWindow.bar.addEventListener("mousedown", function(e) {
	  if ((e.target != vWindow.outerWindow.bar.close) && (e.button == 0)) {
		  e.preventDefault();
		  oldcursorX = e.clientX;
		  oldcursorY = e.clientY;
		  document.addEventListener("mousemove", windowDrag, false);
		  document.addEventListener("mouseup", windowDragEnd, false);
	  }
  }, false);
  
  function windowDrag(e) {
	  e.preventDefault();
	  vWindow.outerWindow.style.left = (vWindow.outerWindow.offsetLeft - (oldcursorX - e.clientX)) + "px";
	  oldcursorX = e.clientX;
	  vWindow.outerWindow.style.top = (vWindow.outerWindow.offsetTop - (oldcursorY - e.clientY)) + "px";
	  oldcursorY = e.clientY;
  }
  
  // pop window back into view area, and set max dimensions if it's not clipping into bottom right
  function windowDragEnd() {
	  document.removeEventListener("mousemove", windowDrag);
	  document.removeEventListener("mouseup", windowDragEnd);
	  vWindow.outerWindow.style.left = Math.min(Math.max(vWindow.outerWindow.offsetLeft, 0 - parseInt(vWindow.outerWindow.style.width) + leftClip), window.innerWidth - rightClip) + "px";
	  vWindow.outerWindow.style.top = Math.min(Math.max(vWindow.outerWindow.offsetTop, topClip), window.innerHeight - bottomClip) + "px";
	  if (!clipRight()) vWindow.outerWindow.style.maxWidth = (window.innerWidth - (parseInt(vWindow.outerWindow.style.left) + (borderThickness * 2))) + "px";
	  if (!clipBottom()) vWindow.outerWindow.style.maxHeight = (window.innerHeight - (parseInt(vWindow.outerWindow.style.top) + (borderThickness * 2))) + "px";
  }
  // end constructor creation
}


var Apps = ['"https://vscode.dev/"','"https://oa-bookmarkos.netlify.app/browser.html"'];
var AppWinOpen = false;
var OpenApps = document.getElementById("OpenApps")

function OpenWindow(code,name,Style,type) {
  var eg = new jswindow({title: name, icon: "",WinType: type, winID: windows});
			  eg.innerWindow.innerHTML = code;
			  eg.innerWindow.style = Style;
			  eg.open({width: 500, height: 300});
			  console.log(windows)
			  var Open = document.createElement("h3")
			  Open.innerHTML = "Win" + windows + "  ";
			  Open.id = "Win"+windows+""
			  Open.className = "Oname"
			  OpenApps.appendChild(Open)

}



function OpenApp(src) {
  OpenWindow("<iframe style='height: 100%; width: 100%; object-fit: contain' src='"+src+"'></iframe>",src)
}

function AppWin() {
  if (AppWinOpen) {return};
  AppWinOpen = true;
  OpenWindow(`+'`'+`
  
  <h1>Apps</h1>
  <br>
<div id="APPS">

  </div>
  
  `+'`'+`,'Apps','text-align: center;','AppWin');

  for (let i = 0; i < Apps.length; i++) {
	  var Btn = "<button onclick='OpenApp("+Apps[i]+")'>"+Apps[i]+"</button>"
	  document.getElementById("APPS").innerHTML += Btn+"<br><br>";

}

}






`;
var OSnav = document.createElement("div");
OSnav.id = "OSnav";
OSnav.innerHTML = `





<div class="OSdropup" id="OSicon">
<button class="OSdropbtn">
  <img src="https://oa-bookmarkos.netlify.app/icon.png" alt="icon" id="OSiconIMG">
  </button>
<div class="OSdropup-content">
  <a href="javascript: AppWin();" id="winTest">Apps</a>
</div>
</div>


<div id="OpenApps"></div>



<style>
  

#OpenApps {
	width: 50%;
	height: 100%;
	background-color: rgb(56, 0, 200);
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);




}
.Oname {

	display: inline;
}

.window {
  resize: both;
  overflow: hidden;
  position: fixed;
  border: 2px solid darkgrey;
  margin: 0px;
  background-color: #DDD;
  cursor: default;
  min-width: 20px;
  min-height: 20px;
  padding: 0px;
  z-index: 1000;
}

.windowbar {
  font-family: sans-serif;
  user-select: none;
  width: calc(100% - 4px);
  padding: 2px;
  height: 16px;
  line-height: 16px;
  color: black;
  background-color: lightblue;
  background-image: url("bar.png");
  font-size: 12px;
  border-bottom: 2px solid darkgrey;
  overflow: hidden;
}

.windowicon, .windowtitle, .windowclose {
  display: inline-block;
}

.windowicon {
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  width: 16px;
  height: 16px;
  position: absolute;
  top: 2px;
  left: 2px;
}

.windowtitle {
  position: relative;
}

.windowicon+.windowtitle {
  left: 18px;
}

.windowclose {
  position: absolute;
  top: 2px;
  right: 2px;
  width: 14px;
  height: 14px;
  line-height: 14px;
  text-align: center;
  background-color: tomato;
  border: 1px outset lightgray;
}

.windowclose:active {
  border-style: inset;
}

.windowcontent {
  width: 100%;
  height: calc(100% - 22px);
  overflow: auto;
}
  #OSnav{
	  position: fixed;
	  bottom: 0;
	  left: 0;
	  width: 100%;
	  height: 35px;
	  background-color: rgb(56, 0, 119);
	  color: white;
	  text-align: center;
	  z-index: 99999999;
  }
  #OSicon{
	  position: absolute;
	  top: 0;
	  left: 0;
  
  }
  #OSiconIMG{
	  position: absolute;
	  top: 0;
	  left: 0;
	  width: 35px;
	  height: 35px;
  }
  .OSdropbtn {
background-color: #3498DB;
color: white;
padding: 16px;
font-size: 16px;
border: none;
}

.OSdropup {


}

.OSdropup-content {
display: none;
position: fixed;
background-color: #f1f1f1;
min-width: 160px;
bottom: 3%;
z-index: 1;
}

.OSdropup-content a {
color: black;
padding: 12px 16px;
text-decoration: none;
display: block;
}

.OSdropup-content a:hover {background-color: #ccc}

.OSdropup:hover .OSdropup-content {
display: block;
}

.OSdropup:hover .OSdropbtn {
background-color: #2980B9;
}
</style>

`
document.body.appendChild(OSnav);
document.body.appendChild(OSJS);
